<?php?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Drupal, Wordpress, Laravel</title>
    <link href="node_modules/bootstrap/dist/css/bootstrap.css" type="text/scss" rel="stylesheet"/>
    <link href="assets/scss/styles.scss" type="text/css" rel="stylesheet"/>


</head>
<a href="">
    <h1 style="margin: 0;font-weight: bold; font-size: 45px;">
        <span class="text-danger">Web</span><span class="" style="color: black;">Pro</span></h1>
</a>
<div class="container">
    <!--  NORMAL NavBar-->
    <nav class="navbar fixed-top navbar-light bg-light">
        <nav class="navbar navbar-expand-lg navbar-light bg-light ml-1" id="navnupp">
            <a href="">
            <h1 style="margin: 0;font-weight: bold; font-size: 45px;">
                <span class="text-danger">Web</span><span class="" style="color: black;">Pro</span></h1>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">

                    <li class="nav-item ml-5">
                        <a class="nav-link ml-5" href="#teenused">Mida pakume <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item ml-5">
                        <a class="nav-link ml-5" href="#tööd">Tehtud tööd</a>
                    </li>
                    <li class="nav-item ml-5">
                        <a class="nav-link ml-5" href="#info">Meist</a>
                    </li>
                    <li class="nav-item ml-5">
                        <a class="nav-link ml-5" href="#praktika">Praktika</a>
                    </li>
                    <div class="ml-5">
                        <a class="btn btn-danger ml-5" href="#lisainfo" role="button">VÕTA ÜHENDUST</a>
                    </div>
                </ul>
            </div>
        </nav>
    </nav>

    <blockquote class="blockquote text-center">
        <div class="" id="algus"></div>
    <br>
        <br>
            <br>
    <div class="container">
        <h1>Veebilahendused sinu ettevõttele</h1>

        <footer class="blockquote-footer">Loome-, hooldame veebilehti, infosüsteeme ja teisi erilahendusi. Oma
            tegemistes peame tähtsaks lihtsust ja kasutusmugavust.
            Meie klientideks lisaks lõppkliendile on ka agentuurid ja teised veebiarendajad.
        </footer>
    </div>
<br>
    <br>
        <br>

    <div class="teenused" id="teenused">
        <br>
            <br>
                <br>
                    <br>
        <h1>Teenused ja Lahendused</h1>
        <div class="card-group">
            <div class="card">
                <img class="card-img-top" src="images/kisspng-drupal-content-management-system-website-developme-drupal-cms-5b7de20839a1a0.1465923115349765202361.png" alt="">
                <div class="card-body">
                    <h3 class="card-title">Drupal arendus</h3>
                    <p class="card-text">Arendame erinevaid Drupalil baseeruvaid süsteeme ja lisamooduleid vastavalt
                        kliendi soovidele ning vajadustele. Pakume ka hooldus ja taastamisteenust juhuks, kui veebileht
                        on häkitud.
                    </p>
                        <button type="button" class="btn btn-info mb-3" onclick="window.location.href='teenused/drupal.html'">VAATA LISAKS</button>
                    <a class="btn btn-danger mb-3" href="#praktika" role="button">KÜSI PAKKUMIST</a>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top"
                     src="images/kissclipart-robot-logo-clipart-robotics-logo-1416af6373412053.png" alt="">
                <div class="card-body">
                    <h3 class="card-title">Tööprotsesside automatiseerimine</h3>
                    <p class="card-text">Vaatleme koos teie tänast tööprotsessi ja pakume välja kaasaegsemaid
                        lahendusi.</p>
                    <button type="button" class="btn btn-info mb-3" onclick="window.location.href='teenused/tooprotsessid.html'">VAATA LISAKS</button>
                    <a class="btn btn-danger mb-3" href="#praktika" role="button">KÜSI PAKKUMIST</a>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="images/Light-Bulb-Transparent-PNG.png" alt="">
                <div class="card-body">
                    <h3 class="card-title">Erilahendused</h3>
                    <p class="card-text">Oleme valmis looma näitena android rakendusi (hübriid / native), infosüsteeme,
                        API backend süsteeme, Wordpress pluginaid.</p>
                    <a class="btn btn-danger" href="#praktika" role="button">KÜSI PAKKUMIST</a>
                </div>
            </div>
        </div>
    </div>

    <br>
        <br>
    <blockquote class="blockquote text-center" id="tööd">
        <br>
        <br></blockquote>
                <br>
        <h1>Valik tehtud veebiarendustest</h1>
    </blockquote>
    <div class="card-group">
        <div class="card" id="drupal">
            <div class="card-body">
                <h3 class="card-title">Drupal</h3>
                <ul>
                    <li><a href="http://masinakeskus.ee">http://masinakeskus.ee</a> - Drupal 8 - Erilahendus html ja administratsioon 2018</li>
                    <li><a href="http://suveniiritehas.ee/">http://suveniiritehas.ee/</a> - 2014</li>
                    <li><a href="http://raamatukodu.ee">http://raamatukodu.ee</a> - Drupal 8 pood 2018</li>
                    <li><a href="http://pegasus.ee">pegasus.ee </a>2008 - Drupal 6 - html, css, transpordilahend, pangalingid</li>
                </ul>
            </div>
        </div>
        <div class="card" id="wordpress">
            <div class="card-body">
                <h3>Wordpress</h3>
                <h5>Veebisaidid</h5>
                <ul>
                    <li><p class="card-text"><a href="http://ank.ee">ank.ee</a></p></li>
                    <li><p class="card-text"><a href="http://konverents.ank.ee">konverents.ank.ee</a></p></li>
                    <li><p class="card-text"><a href="http://epcc.ee">epcc.ee</a></p></li>
                    <li>Pluginad / erilahendused</li>
                    <li><p class="card-text">XLSX import Wordpressi kasutajaliidese kaudu süsteemi ja kaardirakendus 100
                        000 geopunkti</p></li>
                    <li><p class="card-text">Woocommerce e-poe liidestus JSON API</p></li>
                </ul>
            </div>
        </div>
        <div class="card" id="integratsioonid">
            <div class="card-body">
                <h3 class="card-title">Integratsioonid</h3>
                <ul>
                    <li>
                        <a href="#">Drupal Commerce 7</a> + <a href="#">Directo</a>
                    </li>

                    <li>
                        <a href="#">Drupal 6</a> + <a href="#">Mailbow</a>
                    </li>

                    <li>
                        <a href="#">Drupal Commerce 7 </a>+ <a href="#">Itella Smartpost</a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    <br>
        <br>
<div class="card-body" id="java">
    <h3 class="card-title">Javascript (erilahendused)</h3>
    <ul>
        <li>
            <a href="https://ckeditor.com/cke4/addon/btgrid">https://ckeditor.com/cke4/addon/btgrid</a>
        </li>

        <li>
            <a href="https://ckeditor.com/cke4/addon/bttable">https://ckeditor.com/cke4/addon/bttable</a>
        </li>

        <li>
            <a href="https://ckeditor.com/cke4/addon/btquicktable">https://ckeditor.com/cke4/addon/btquicktable</a>
         </li>
    </ul>
    <blockquote class="blockquote text-center">
        <br>
            <br>
                <br>

        <div class="meist" id="info">
            <br>
                <br>
                    <br>
            <h1>Meist</h1>
            <footer class="blockquote-footer">Tegemist on väikese tegusa kollektiiviga Viljandist. Arendame

                põhiliselt <a href="#">Drupal</a> projekte, kuid ei ültle ära ka <a href="#">Wordpress</a>,<a
                        href="#"> Laravel</a> või näiteks Android <br>(telefoniäppi) arendusest.
            </footer>
        </div>
        <br>
             <br>
    </blockquote>
    <div class="praktika" id="praktika">
        <br>
            <br>
                <br>
        <blockquote class="blockquote text-center">
            <br>
            <h1>Praktika</h1>
            <footer class="blockquote-footer">Paku end praktikale, kui oled huvitatud veebiarendusest ja otsid
                väljakutseid ning soovid end proovile panna.
                <br>
            </footer>
                <br>
            <footer class="blockquote-footer">Praktika läbiviimine Viljandi linnas asuvas kontoris.</footer>
            <br>
            <footer class="blockquote-footer">Praktikasoovi korral Veebiarendaja OÜ-s võtke ühendust telefonil:
                <a href="#">+37253533822</a></footer>
        </blockquote>
    </div>
</div>
</div>



<footer id="footer">
    <div class="container">
        <h1>Saada päring</h1>
        <div class="row">
            <div class="col-sm-9">
                <form>
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="email" class="form-control mb-4" id="email" placeholder="Email">
                            <input type="text" class="form-control mb-4" id="eesnimi" placeholder="Eesnimi">
                            <input type="text" class="form-control mb-4" id="telefon" placeholder="Telefon">
                        </div>
                        <div class="col-sm-6">
                            <textarea class="form-control" rows="5" placeholder="Lisainfo"></textarea>
                            <div class="mt-2">
                            <button type="button" class="btn btn-primary">Saada Ära</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-3" id="lisainfo">
                <h4>Kontaktinfo</h4>
                <ul class="list-unstyled mb-0">
                    <li><i class="fas fa-map-marker-alt fa-2x"></i>
                        <p>Kaido Toomingas</p>
                    </li>

                    <li><i class="fas fa-phone mt-4 fa-2x"></i>
                        <p>kaido@webpro.ee</p>
                    </li>

                    <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                        <p>+372 53 533 822</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Bootstrapi Jquery -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Bootstrapi Popper.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<!-- Bootsbrap ise-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<!-- The jQuery library -->
<script src="node_modules/jquery/dist/jquery.min.js"></script>

<!-- The Moment.js library -->
<script src="node_modules/moment/moment.js"></script>

<!-- Including our own JavaScript file -->
<script src="assets/js/script.js"></script>

</html>
