<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require __DIR__ . '/vendor/autoload.php';
$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/views');
$twig = new \Twig\Environment($loader,
 [
 //   'cache' => __DIR__ . '/compilation_cache',
]);
$ext = new Kint\Twig\TwigExtension();
$ext->setAliases([
    // Function name => Renderer
    'dump' => 'Kint\\Renderer\\RichRenderer',
]);
$twig->addExtension($ext);

$web_json = file_get_contents('https://www.webpro.ee/web.json');
$web = json_decode($web_json);
$web_json2 = file_get_contents('https://www.webpro.ee/services.json');
$services = json_decode($web_json2);
$web_json3 = file_get_contents('https://www.webpro.ee/references.json');
$references = json_decode($web_json3);
#Kint::dump($web);
#Kint::dump($services);
#Kint::dump($references);
$ref_group = [];
foreach ($references as $k => $reference) {
  $ref_group[$reference->group][] = $reference;     
}
#Kint::dump($ref_group);
echo $twig->render('index.twig', ['data' => $web, 'services' => $services, 'references' => $ref_group]);
?>